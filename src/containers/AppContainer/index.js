import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from '../../actions/predictionActions';

import App from '../../components/App/';

class AppContainer extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <App {...this.props} />
        );
    }

}

function mapStateToProps(state) {
    return {
        number: state.predictionReducer.number,
        loaded: state.predictionReducer.loaded,
        words: state.predictionReducer.words,
        error: state.predictionReducer.error,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        predictionActions: bindActionCreators(actions, dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(AppContainer);

AppContainer.propTypes = {
    predictionActions: PropTypes.object.isRequired,
    number: PropTypes.string.isRequired,
    loaded: PropTypes.bool.isRequired,
    words: PropTypes.array.isRequired,
    error: PropTypes.string.isRequired,
};
