import predictionSaga from '../sagas/predictionSaga';

export default function* () {
    yield [
        predictionSaga(),
    ];
}
