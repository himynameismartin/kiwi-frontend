import { takeLatest, put, call, select } from 'redux-saga/effects';

import * as types from '../constants/PredictionTypes';
import { getNumber } from '../selectors/predictionSelectors'
import { predictionFailed, predictionSuccess, predictionSkipped } from '../actions/predictionActions';
import { predictionApi } from '../api/predictionApi';

function* fetchRecords(action) {

    const number = yield select(getNumber);

    if (number.length) try {
        const response = yield call(predictionApi.fetchData, number);

        if (response.words) {
            return yield put(predictionSuccess(response.words));
        }
        return yield put(predictionFailed(response.message));
    } catch (e) {
        return yield put(predictionFailed(JSON.stringify(e)));
    }

    return yield put(predictionSkipped());
}

export default function* predictionSaga() {
    yield takeLatest(types.ON_KEY_PRESS, fetchRecords)
}
