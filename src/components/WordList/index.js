import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Chip from 'material-ui/Chip';

const styles = {
    chip: {
        margin: 4,
    },
    wrapper: {
        display: 'flex',
        flexWrap: 'wrap',
    },
};

export default class WordList extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div style={styles.wrapper}>
                {
                    this.props.words.length ?
                        this.props.words.map((word) => (
                            <Chip
                                style={styles.chip}
                                key={word}
                            >
                                {word}
                            </Chip>
                        )) : null
                }
            </div>
        )
    }
}

WordList.propTypes = {
    words: PropTypes.array.isRequired,
};
