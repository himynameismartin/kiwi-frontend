import React, { Component } from 'react';
import PropTypes from 'prop-types';

const styles = {
    error: {
        color: 'red',
        fontSize: '1.1em',
    },
};

export default class Notification extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div style={styles.error}>
                <strong>{this.props.error}</strong>
            </div>
        )
    }
}

Notification.propTypes = {
    error: PropTypes.string.isRequired,
};
