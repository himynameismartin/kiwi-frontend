import React, { Component } from 'react';
import PropTypes from 'prop-types';

import loader from 'hoc-react-loader';

import { Card, CardHeader, CardText } from 'material-ui/Card';

import KeyPadList from '../KeyPadList/';
import Notification from '../Notification/';
import WordList from '../WordList/';

class App extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Card>
                <CardHeader
                    title="Enter any number and get a list of corresponding T9 words"
                    subtitle={this.props.number || (<em>e.g. <strong>5494</strong> ;)</em>)}
                />
                <CardText>
                    <KeyPadList predictionActions={this.props.predictionActions} />

                    <Notification error={this.props.error} />

                    <WordList words={this.props.words} />
                </CardText>
            </Card>
        )
    }
}

export default loader({ print: ['loaded'] })(App);

App.propTypes = {
    predictionActions: PropTypes.object.isRequired,
    number: PropTypes.string.isRequired,
    loaded: PropTypes.bool.isRequired,
    words: PropTypes.array.isRequired,
    error: PropTypes.string.isRequired,
};
