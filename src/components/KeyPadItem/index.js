import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { GridTile } from 'material-ui/GridList';
import { Card, CardHeader } from 'material-ui/Card';

export default class KeyPadItem extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <GridTile
                onClick={() => {this.props.predictionActions.onKeyPress(this.props.item.main)}}
            >
                <Card>
                    <CardHeader
                        title={this.props.item.main}
                        subtitle={this.props.item.minor.join('')}
                    />
                </Card>
            </GridTile>
        )
    }
}

KeyPadItem.propTypes = {
    predictionActions: PropTypes.object.isRequired,
    item: PropTypes.object.isRequired,
};
