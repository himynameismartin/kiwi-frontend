import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { GridList } from 'material-ui/GridList';

import KeyPadItem from '../KeyPadItem/';

import { KEY_PADS } from '../../constants/KeyPads';

const styles = {
    gridList: {
        width: 225,
        overflowY: 'auto',
    },
};

export default class KeyPadList extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <GridList
                cellHeight={50}
                cols={3}
                padding={5}
                style={styles.gridList}
            >
                {KEY_PADS.map((key) => (
                    <KeyPadItem key={key.main} item={key} predictionActions={this.props.predictionActions} />
                ))}
            </GridList>
        )
    }
}

KeyPadList.propTypes = {
    predictionActions: PropTypes.object.isRequired,
};
