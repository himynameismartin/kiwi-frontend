import { createStore, applyMiddleware, compose } from 'redux';

import createSagaMiddleware from 'redux-saga';

import rootReducer from '../reducers';
import rootSaga from '../sagas';

export default function configureStore(initialState) {
    const sagaMiddleware = createSagaMiddleware();

    const middleware = applyMiddleware(sagaMiddleware);
    const enhancer = compose(middleware);

    const store = createStore(rootReducer, initialState, enhancer);

    sagaMiddleware.run(rootSaga);

    return store;
}
