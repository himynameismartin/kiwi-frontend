import * as types from '../constants/PredictionTypes';

export function predictionSkipped() {
    return {
        type: types.GET_PREDICTION_SKIPPED,
    };
}

export function predictionSuccess(words) {
    return {
        type: types.GET_PREDICTION_SUCCESS,
        words,
    };
}

export function predictionFailed(error) {
    return {
        type: types.GET_PREDICTION_FAILED,
        error,
    };
}

export function onKeyPress(key) {
    return {
        type: types.ON_KEY_PRESS,
        key,
    };
}
