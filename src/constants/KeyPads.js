export const KEY_PADS = [
    {
        main: '1',
        minor: ['_'],
    },
    {
        main: '2',
        minor: ['a', 'b', 'c'],
    },
    {
        main: '3',
        minor: ['d', 'e', 'f'],
    },
    {
        main: '4',
        minor: ['g', 'h', 'i'],
    },
    {
        main: '5',
        minor: ['j', 'k', 'l'],
    },
    {
        main: '6',
        minor: ['m', 'n', 'o'],
    },
    {
        main: '7',
        minor: ['p', 'q', 'r', 's'],
    },
    {
        main: '8',
        minor: ['t', 'u', 'v'],
    },
    {
        main: '9',
        minor: ['w', 'x', 'y', 'z'],
    },
    {
        main: 'r',
        minor: [''],
    },
    {
        main: '0',
        minor: [''],
    },
    {
        main: 'c',
        minor: [''],
    },
];
