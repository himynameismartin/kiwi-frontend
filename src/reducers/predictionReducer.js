import * as types from '../constants/PredictionTypes';

const initialState = {
    number: "",
    loaded: true,
    words: [],
    error: "",
};

export default function (state = initialState, action) {
    switch (action.type) {
        case types.ON_KEY_PRESS:
            let number = state.number;

            if (/^[0-9]$/.test(action.key)) {
                number += action.key;
            } else if (action.key === 'r') {
                number = initialState.number;
            } else if (action.key === 'c') {
                number = number.slice(0, -1);
            }

            return Object.assign({}, state, {
                loaded: false,
                number: number,
                words: initialState.words,
                error: initialState.error,
            });

        case types.GET_PREDICTION_SUCCESS:
            return Object.assign({}, state, {
                loaded: true,
                words: action.words,
            });

        case types.GET_PREDICTION_FAILED:
            return Object.assign({}, state, {
                loaded: true,
                words: initialState.words,
                error: action.error
            });

        case types.GET_PREDICTION_SKIPPED:
            return Object.assign({}, state, {
                loaded: true,
                words: initialState.words,
                error: initialState.error
            });

        default:
            return state;
    }
}
