import { combineReducers } from 'redux';
import predictionReducer from './predictionReducer';

const rootReducer = combineReducers({
    predictionReducer,
});

export default rootReducer;
