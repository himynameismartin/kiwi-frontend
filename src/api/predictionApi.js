export const predictionApi = {
    fetchData(number) {
        return fetch(`http://localhost:8000/words/${number}`)
        .then(response => response.json())
        .catch(error => error)
        .then(data => {
             return data
         })
    }
}
